package com.mt.ws2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.mt.ws2.model.Cat;

@RestController
public class HelloController {
    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Autowired
    RestTemplate restTemplate;
    
    @CrossOrigin(origins="*")
    @RequestMapping("/")
    public @ResponseBody Cat[] hello(){
        Cat[] cats = new Cat[2];
        
        cats[0] = restTemplate.getForObject("http://exemplo-ms-1:8080", Cat.class);
        cats[1] = restTemplate.getForObject("http://exemplo-ms-1:8080", Cat.class);

        return cats;
    }
}
